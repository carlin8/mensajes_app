package com.messagesapp.messages_app;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author consultoria
 */
public class Conexion {

    public Connection get_connection() throws ClassNotFoundException {
        Connection conection = null;
        Class.forName("com.mysql.cj.jdbc.Driver");

        try {
            String host = "192.168.64.2";
            String puerto = "3306";
            String nombreBd = "mensajes_app";
            String usuario = "usuario";
            String password = "usuario";

            conection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + puerto + "/" + nombreBd, usuario, password);
            if (conection != null) {
                System.out.print("Conexion exitosa \n\n");
            }
            Statement stmt = conection.createStatement();
            ResultSet rs = stmt.executeQuery("SHOW VARIABLES LIKE \"%version%\";");
            while (rs.next()) {
                String name = rs.getString("variable_name");
                String value = rs.getString("Value");
                System.out.println(name + ": \t\t\t" + value + "\n");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return conection;

    }

}
		